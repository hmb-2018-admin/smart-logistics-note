 1.指标统计之运单主题
   
   1).lg_ods层建表
   建表语句
   --运单表
DROP TABLE IF EXISTS `lg_ods.lg_waybill`;
CREATE TABLE `lg_ods.lg_waybill` (
`id` string,
`express_bill_number` string,
`waybill_number` string,
`cid` string,
`eid` string,
`order_channel_id` string,
`order_dt` string,
`order_terminal_type` string,
`order_terminal_os_type` string,
`reserve_dt` string,
`is_collect_package_timeout` string,
`pkg_id` string,
`pkg_number` string,
`timeout_dt` string,
`transform_type` string,
`delivery_customer_name` string,
`delivery_addr` string,
`delivery_mobile` string,
`delivery_tel` string,
`receive_customer_name` string,
`receive_addr` string,
`receive_mobile` string,
`receive_tel` string,
`cdt` string,
`udt` string,
`remark` string
) COMMENT '运单表'
PARTITIONED BY (`dt` string) row format delimited fields terminated by ',';
   
   --转运记录表:lg_transport_record
DROP TABLE IF EXISTS `lg_ods.lg_transport_record`;
CREATE TABLE `lg_ods.lg_transport_record` (
`id` string,
`pw_id` string,
`pw_waybill_id` string,
`pw_waybill_number` string,
`ow_id` string,
`ow_waybill_id` string,
`ow_waybill_number` string,
`sw_id` string,
`ew_id` string,
`transport_tool_id` string,
`pw_driver1_id` string,
`pw_driver2_id` string,
`pw_driver3_id` string,
`ow_driver1_id` string,
`ow_driver2_id` string,
`ow_driver3_id` string,
`route_id` string,
`distance` string,
`duration` string,
`state` string,
`start_vehicle_dt` string,
`predict_arrivals_dt` string,
`actual_arrivals_dt` string,
`cdt` string,
`udt` string,
`remark` string
)COMMENT '转运记录表'
PARTITIONED BY (`dt` string) row format delimited fields terminated by ',';
   
   --线路表:lg_route
DROP TABLE IF EXISTS `lg_ods.lg_route`;
CREATE TABLE `lg_ods.lg_route` (
`id` string,
`start_station` string,
`start_station_area_id` string,
`start_warehouse_id` string,
`end_station` string,
`end_station_area_id` string,
`end_warehouse_id` string,
`mileage_m` string,
`time_consumer_minute` string,
`state` string,
`cdt` string,
`udt` string,
`remark` string
) COMMENT '线路表'
PARTITIONED BY (`dt` string) row format delimited fields terminated by ',';
   
   --车辆表:lg_transport_tool
DROP TABLE IF EXISTS `lg_ods.lg_transport_tool`;
CREATE TABLE `lg_ods.lg_transport_tool` (
`id` string,
`brand` string,
`model` string,
`type` string,
`given_load` string,
`load_cn_unit` string,
`load_en_unit` string,
`buy_dt` string,
`license_plate` string,
`state` string,
`cdt` string,
`udt` string,
`remark` string
) COMMENT '车辆表'
PARTITIONED BY (`dt` string) row format delimited fields terminated by ',';
   2).数据采集
   业务数据保存在MySQL中，每日凌晨导入上一天的表数据。
   事实表
	   全量导入
#!/bin/bash
source /etc/profile
##如果第一个参数不为空，则作为工作日期使用
if [ -n "$1" ]
then
do_date=$1
else
##昨天日期，减一
do_date=`date -d "-1 day" +"%Y%m%d"`
fi
#定义sqoop命令位置，Hive命令位置，在hadoop2
#sqoop=/opt/cloudera/parcels/CDH/bin/sqoop
#Hive=/opt/cloudera/parcels/CDH/bin/hive
#定义工作日期

#编写导入数据通用方法 接收两个参数：第一个：表名，第二个：查询语句
import_data(){
sqoop import \
--connect jdbc:mysql://linux123:3306/lg_logistics \
--username root \
--password 12345678 \
--target-dir /user/hive/warehouse/lg_ods.db/$1/dt=$do_date \
--delete-target-dir \
--query "$2 and \$CONDITIONS" \
--num-mappers 1 \
--fields-terminated-by ',' \
--null-string '\\N' \
--null-non-string '\\N'
}

# 全量导入运单表数据
import_lg_waybill(){
import_data lg_waybill "select
* 
from lg_waybill
where 1=1"
}

# 全量导入转运记录表
import_lg_transport_record(){
import_data lg_transport_record "select
* 
from lg_transport_record
where 1=1"
}

#调用全量导入数据方法
import_lg_waybill
import_lg_transport_record

#注意sqoop导入数据的方式，对于Hive分区表来说需要执行添加分区操作，数据才能被识别到
hive -e "alter table lg_ods.lg_waybill add partition(dt='$do_date');
alter table lg_ods.lg_transport_record add partition(dt='$do_date');
"  
   
        增量导入
#!/bin/bash
source /etc/profile
##如果第一个参数不为空，则作为工作日期使用
if [ -n "$1" ]
then
do_date=$1
else
##昨天日期，减一
do_date=`date -d "-1 day" +"%Y%m%d"`
fi
#定义sqoop命令位置，Hive命令位置，在hadoop2
#sqoop=/opt/cloudera/parcels/CDH/bin/sqoop
#Hive=/opt/cloudera/parcels/CDH/bin/hive
#定义工作日期

#编写导入数据通用方法 接收两个参数：第一个：表名，第二个：查询语句
import_data(){
sqoop import \
--connect jdbc:mysql://linux123:3306/lg_logistics \
--username root \
--password 12345678 \
--target-dir /user/hive/warehouse/lg_ods.db/$1/dt=$do_date \
--delete-target-dir \
--query "$2 and \$CONDITIONS" \
--num-mappers 1 \
--fields-terminated-by ',' \
--null-string '\\N' \
--null-non-string '\\N'
}

# 增量导入运单数据
import_lg_waybill(){
import_data lg_waybill "select
*
from lg_waybill
WHERE DATE_FORMAT(cdt, '%Y%m%d') = '${do_date}'
"
}

# 增量导入转运记录
import_lg_transport_record(){
import_data lg_transport_record "select
* 
from lg_transport_record
WHERE DATE_FORMAT(cdt, '%Y%m%d') = '${do_date}'
"
}

#调用全量导入数据方法
import_lg_waybill
import_lg_transport_record

#注意sqoop导入数据的方式，对于Hive分区表来说需要执行添加分区操作，数据才能被识别到
hive -e "alter table lg_ods.lg_express_bill add partition(dt='$do_date');
alter table lg_ods.lg_pkg add partition(dt='$do_date');
"  
   维度表
       全量导入
#!/bin/bash
source /etc/profile
##如果第一个参数不为空，则作为工作日期使用
if [ -n "$1" ]
then
do_date=$1
else
##昨天日期，减一
do_date=`date -d "-1 day" +"%Y%m%d"`
fi
#定义sqoop命令位置，Hive命令位置，在hadoop2
#sqoop=/opt/cloudera/parcels/CDH/bin/sqoop
#Hive=/opt/cloudera/parcels/CDH/bin/hive
#定义工作日期

#编写导入数据通用方法 接收两个参数：第一个：表名，第二个：查询语句
import_data(){
sqoop import \
--connect jdbc:mysql://linux123:3306/lg_logistics \
--username root \
--password 12345678 \
--target-dir /user/hive/warehouse/lg_ods.db/$1/dt=$do_date \
--delete-target-dir \
--query "$2 and \$CONDITIONS" \
--num-mappers 1 \
--fields-terminated-by ',' \
--null-string '\\N' \
--null-non-string '\\N'
}

# 全量导入线路表
import_lg_route(){
import_data lg_route "select
* 
from lg_route
where 1=1"
}

# 全量导入车辆表
import_lg_transport_tool(){
import_data lg_transport_tool "select
* 
from lg_transport_tool
where 1=1"
}

#调用全量导入数据方法
import_lg_route
import_lg_transport_tool

#注意sqoop导入数据的方式，对于Hive分区表来说需要执行添加分区操作，数据才能被识别到
hive -e "alter table lg_ods.lg_route add partition(dt='$do_date');
alter table lg_ods.lg_transport_tool add partition(dt='$do_date');
"	   
   3).ETL
   (1).指标明细
   指标列表   维度
   运单数     总运单数
   最大运单数 最大区域运单数
              各分公司最大运单数
              各网点最大运单数
              各线路最大运单数
              各运输工具最大运单数
              各类客户最大运单数
   最小运单数 各区域最小运单数
              各分公司最小运单数
              各网点最小运单数
              各线路最小运单数
              各运输工具最小运单数
              各类客户最小运单数
   平均运单数 各区域平均运单数
              各分公司平均运单数
              各网点平均运单数
              各线路平均运单数
              各运输工具平均运单数
              各类客户平均运单数
   (2).表关系示意图
   事实表
   表名                描述
   lg_waybill          运单表
   lg_transport_record 转运记录表
   
   维度表
   表名 描述
   lg_courier        快递员表
   lg_areas          区域表
   lg_route          线路表
   lg_dot            网点表
   lg_company        公司表
   lg_customer       客户表
   lg_transport_tool 车辆表
   lg_codes 物流系统码表
   (3).预处理
   1 、事实表
   运单与转运记录宽表
   drop table if exists lg_dwd.waybill_way_tran_record;
create table lg_dwd.waybill_way_tran_record(
express_bill_number string,
waybill_number string,
cid string,
eid string,
order_channel_id string,
order_dt string,
order_terminal_type string,
order_terminal_os_type string,
reserve_dt string,
is_collect_package_timeout string,
pkg_id string,
pkg_number string,
timeout_dt string,
transform_type string,
delivery_customer_name string,
delivery_addr string,
delivery_mobile string,
delivery_tel string,
receive_customer_name string,
receive_addr string,
receive_mobile string,
receive_tel string,
pw_id string,
pw_waybill_id string,
pw_waybill_number string,
ow_id string,
ow_waybill_id string,
ow_waybill_number string,
sw_id string,
ew_id string,
transport_tool_id string,
pw_driver1_id string,
pw_driver2_id string,
pw_driver3_id string,
ow_driver1_id string,
ow_driver2_id string,
ow_driver3_id string,
route_id string,
distance string,
duration string,
state string,
start_vehicle_dt string,
predict_arrivals_dt string,
actual_arrivals_dt string
)COMMENT '运单主题-运单与转运记录事实表'
partitioned by (dt string) STORED AS PARQUET ;
   
   --插入数据
insert overwritetable lg_dwd.waybill_way_tran_record partition(dt='2020-06-
02')
select                  
t1.express_bill_number,
t1.waybill_number,
t1.cid,
t1.eid,
t1.order_channel_id,
t1.order_dt,
t1.order_terminal_type,
t1.order_terminal_os_type,
t1.reserve_dt,
t1.is_collect_package_timeout,   
t1.pkg_id, 
t1.pkg_number, 
t1.timeout_dt, 
t1.transform_type, 
t1.delivery_customer_name , 
t1.delivery_addr, 
t1.delivery_mobile, 
t1.delivery_tel, 
t1.receive_customer_name, 
t1.receive_addr, 
t1.receive_mobile, 
t1.receive_tel,             
t2.pw_id, 
t2.pw_waybill_id, 
t2.pw_waybill_number, 
t2.ow_id, 
t2.ow_waybill_id, 
t2.ow_waybill_number, 
t2.sw_id, 
t2.ew_id, 
t2.transport_tool_id, 
t2.pw_driver1_id, 
t2.pw_driver2_id, 
t2.pw_driver3_id, 
t2.ow_driver1_id, 
t2.ow_driver2_id, 
t2.ow_driver3_id, 
t2.route_id, 
t2.distance, 
t2.duration, 
t2.state, 
t2.start_vehicle_dt, 
t2.predict_arrivals_dt, 
t2.actual_arrivals_dt
from (select * from lg_ods.lg_waybill where dt ='2020-06-02') t1
left join (select * from lg_ods.lg_transport_record where dt ='2020-06-02') t2
on t1.waybill_number =t2.pw_waybill_number ;
   
   
   2 、维度表
   创建客户字典宽表
   drop table if exists dim.waybill_customer_codes;
create table dim.waybill_customer_codes (
id string,
name string,
tel string,
mobile string,
email string,
type string,
is_own_reg string,
reg_dt string,
reg_channel_id string,
state string,
last_login_dt string,
code_name string,
code_type string,
code string,
code_desc string,
code_cust_type string,
codes_state string 
)COMMENT '运单主题-客户字典宽表'
partitioned by (dt string) STORED AS PARQUET 
   
   insert overwrite table dim.waybill_customer_codes partition(dt='2020-06-02')
select
t1.id,t1.name,
t1.tel,
t1.mobile,
t1.email,
t1.type,
t1.is_own_reg,
t1.reg_dt,
t1.reg_channel_id,
t1.state,                 
t1.last_login_dt,                                   
t2.name as code_name, 
t2.type as code_type, 
t2.code, 
t2.code_desc, 
t2.code_type as code_cust_type, 
t2.state as codes_state 
from (select * from lg_ods.lg_customer where dt ='2020-06-02') t1
left join (select * from lg_ods.lg_codes where dt ='2020-06-02' and type ='1')
t2
on t1.type =t2.type ;
   
   创建网点区域宽表
   drop table if exists dim.waybill_courier_dot_area;
create table dim.waybill_courier_dot_area (
id string,
job_num string,
name string,
birathday string,
tel string,
pda_num string,
car_id string,
postal_standard_id string,
work_time_id string,
dot_id string,
state string,
dot_number string,
dot_name string,
dot_addr string,
dot_gis_addr string,
dot_tel string,
company_id string,
manage_area_id string,
manage_area_gis string,
dot_state string,
area_name string,
pid string,
sname string,
level string,
citycode string,
yzcode string,
mername string,
lng string,
lat string,
pinyin string) COMMENT '运单主题-快递员网点区域宽表'
partitioned by (dt string) STORED AS PARQUET ; 
   
   insert overwrite table dim.waybill_courier_dot_area partition(dt='2020-06-02')
select
t1.id,
t1.job_num,
t1.name,
t1.birathday,
t1.tel,
t1.pda_num,
t1.car_id,
t1.postal_standard_id,
t1.work_time_id,
t1.dot_id,
t1.state,      
t2.dot_number,
t2.dot_name,
t2.dot_addr,
t2.dot_gis_addr,
t2.dot_tel,
t2.company_id ,
t2.manage_area_id,
t2.manage_area_gis,
t2.state as dot_state,      
            
t3.name as area_name,
t3.pid,
t3.sname,
t3.level,
t3.citycode,
t3.yzcode,
t3.mername,
t3.lng,
t3.lat,
t3.pinyin                       
from (select * from lg_ods.lg_courier where dt ='2020-06-02' ) t1
left join (select * from lg_ods.lg_dot where dt ='2020-06-02' ) t2
on t1.dot_id=t2.id
left join (select * from lg_ods.lg_areas where dt ='2020-06-02' ) t3
on t2.manage_area_id =t3.id;
   线路表
   车辆表
   4).指标统计
   计算指标
   指标描述
   最大区域运单数
   最小区域运单数
   各网点最大运单数
   各网点最小运单数
   各线路最大运单数
   各线路最小运单数
   各运输工具最大运单数
   各运输工具最小运单数
   各类客户最大运单数
   各类客户最小运单数
   
   --创建lg_dws层表
drop table if exists lg_dws.waybill_base_agg;
create table lg_dws.waybill_base_agg
(waybill_count bigint,
area_name string,
dot_id string,
dot_name string,
route_id string,
tool_id string,
cus_type string)COMMENT '运单主题-lg_dws基础汇总表'
partitioned by (dt string) STORED AS PARQUET ;
   
   insert overwrite table lg_dws.waybill_base_agg partition(dt='2020-06-02')
select
count(record.waybill_number) as waybill_count,
courier_area.area_name as area_name,
courier_area.dot_id as dot_id,
courier_area.dot_name as dot_name,
route.id as route_id,
tran_tool.id as tool_id,
cus_codes.type as cus_type
from
(select * from lg_dwd.waybill_way_tran_record where dt ='2020-06-02') record
left join
(select * from dim.waybill_customer_codes where dt='2020-06-02') cus_codes
on record.cid= cus_codes.id
left join
(select * from dim.waybill_courier_dot_area where dt='2020-06-02') courier_area
on record.eid= courier_area.id
left join
(select * from lg_ods.lg_route where dt ='2020-06-02') route
on record.route_id =route.id
left join
(select * from lg_ods.lg_transport_tool where dt ='2020-06-02') tran_tool
on record.transport_tool_id =tran_tool.id
group by
courier_area.area_name,courier_area.dot_id,courier_area.dot_name,route.id,tran_t
ool.id,cus_codes.type;

   指标统计
   --运单数
   --各区域最大运单数 
select sum(t.waybill_count) as area_max_count, t.area_name from (select * from
lg_dws.waybill_base_agg where dt ='2020-06-02') t group by t.area_name  order by
area_max_count desc limit 1;

   --各网点最大运单数
select sum(t.waybill_count) as dot_max_count, t.dot_name, t.dot_id from (select
* from lg_dws.waybill_base_agg where dt ='2020-06-02') t group by
t.dot_id,t.dot_name  order by dot_max_count desc limit 1;
   
   --各线路最大运单数
select sum(t.waybill_count) as route_max_count, t.route_id from (select * from
lg_dws.waybill_base_agg where dt ='2020-06-02') t group by t.route_id  order by
route_max_count desc limit 1;
   
   --各运输工具最大运单数
select sum(t.waybill_count) as tool_max_count, t.tool_id from (select * from
lg_dws.waybill_base_agg where dt ='2020-06-02') t group by t.tool_id  order by
tool_max_count desc limit 1;
   
   --各类客户最大运单数
select sum(t.waybill_count) as cus_max_count, t.cus_type from (select * from
lg_dws.waybill_base_agg where dt ='2020-06-02') t group by t.cus_type  order by
cus_max_count desc limit 1;

   --各区域最小运单数
select sum(t.waybill_count) as area_min_count, t.area_name from (select * from
lg_dws.waybill_base_agg where dt ='2020-06-02') t group by t.area_name  order by
area_min_count asc limit 1;

   --各网点最小运单数
select sum(t.waybill_count) as dot_min_count, t.dot_name, t.dot_id from (select
* from lg_dws.waybill_base_agg where dt ='2020-06-02') t group by
t.dot_id,t.dot_name  order by dot_min_count asc limit 1;
   
   --各线路最小运单数
select sum(t.waybill_count) as route_min_count, t.route_id from (select * from
lg_dws.waybill_base_agg where dt ='2020-06-02') t group by t.route_id  order by
route_min_count asc limit 1;

   --各运输工具最小运单数
select sum(t.waybill_count) as tool_min_count, t.tool_id from (select * from
lg_dws.waybill_base_agg where dt ='2020-06-02') t group by t.tool_id  order by
tool_min_count desc limit 1;
   
   --各类客户最小运单数
select sum(t.waybill_count) as cus_min_count, t.cus_type from (select * from
lg_dws.waybill_base_agg where dt ='2020-06-02') t group by t.cus_type  order by
cus_min_count asc limit 1;
   
   --创建lg_ads层表
drop table if exists lg_ads.waybill_metrics;
create table lg_ads.waybill_metrics(
area_max_count bigint,
area_max_name string,
dot_max_count bigint,
dot_max_name string,
route_max_count bigint,
route_max_id string,
tool_max_count bigint,
tool_max_id string,
cus_max_count bigint,
cus_max_type string,
area_min_count bigint,
area_min_name string,
dot_min_count bigint,
dot_min_name string,
route_min_count bigint,
route_min_id string,
tool_min_count bigint,
tool_min_id string,
cus_min_count bigint,
cus_min_type string ,
dt string
)COMMENT '运主题-指标表' row format delimited fields terminated by ',' STORED AS
textfile ;

   --汇总sql
insert overwrite table lg_ads.waybill_metrics
select
t1.area_max_count,
t1.area_name as area_max_name,
t2.dot_max_count,
t2.dot_name as dot_max_name,
t3.route_max_count,
t3.route_id as route_max_id,
t4.tool_max_count,
t4.tool_id as tool_max_id,
t5.cus_max_count,
t5.cus_type as cus_max_type,
t6.area_min_count,
t6.area_name as area_min_name,
t7.dot_min_count,
t7.dot_name as dot_min_name,
t8.route_min_count,
t8.route_id as route_min_id,
t9.tool_min_count,
t9.tool_id as tool_min_id,
t10.cus_min_count,
t10.cus_type as cus_min_type,
'2020-06-02'
from
(select sum(t.waybill_count) as area_max_count, t.area_name from (select * from
lg_dws.waybill_base_agg where dt ='2020-06-02') t group by t.area_name  order by
area_max_count desc limit 1) t1
join
(select sum(t.waybill_count) as dot_max_count, t.dot_name, t.dot_id from (select
* from lg_dws.waybill_base_agg where dt ='2020-06-02') t group by
t.dot_id,t.dot_name  order by dot_max_count desc limit 1) t2
join
(select sum(t.waybill_count) as route_max_count, t.route_id from (select * from
lg_dws.waybill_base_agg where dt ='2020-06-02') t group by t.route_id  order by
route_max_count desc limit 1) t3
join
(select sum(t.waybill_count) as tool_max_count, t.tool_id from (select * from
lg_dws.waybill_base_agg where dt ='2020-06-02') t group by t.tool_id  order by
tool_max_count desc limit 1) t4
join
(select sum(t.waybill_count) as cus_max_count, t.cus_type from (select * from
lg_dws.waybill_base_agg where dt ='2020-06-02') t group by t.cus_type  order by
cus_max_count desc limit 1) t5
join
(select sum(t.waybill_count) as area_min_count, t.area_name from (select * from
lg_dws.waybill_base_agg where dt ='2020-06-02') t group by t.area_name  order by
area_min_count asc limit 1) t6
join
(select sum(t.waybill_count) as dot_min_count, t.dot_name, t.dot_id from (select
* from lg_dws.waybill_base_agg where dt ='2020-06-02') t group by
t.dot_id,t.dot_name  order by dot_min_count asc limit 1) t7
join
(select sum(t.waybill_count) as route_min_count, t.route_id from (select * from
lg_dws.waybill_base_agg where dt ='2020-06-02') t group by t.route_id  order by
route_min_count asc limit 1) t8
join
(select sum(t.waybill_count) as tool_min_count, t.tool_id from (select * from
lg_dws.waybill_base_agg where dt ='2020-06-02') t group by t.tool_id  order by
tool_min_count desc limit 1) t9
join
(select sum(t.waybill_count) as cus_min_count, t.cus_type from (select * from
lg_dws.waybill_base_agg where dt ='2020-06-02') t group by t.cus_type  order by
cus_min_count asc limit 1) t10;

   导出指标数据
   创建Mysql指标表
   CREATE TABLE `waybill_agg_metrics` (
`area_max_count` bigint(20) DEFAULT NULL,
`area_max_name` varchar(50) DEFAULT NULL,
`dot_max_count` bigint(20) DEFAULT NULL,
`dot_max_name` varchar(50) DEFAULT NULL,
`route_max_count` bigint(20) DEFAULT NULL,
`route_max_id` varchar(25) DEFAULT NULL,
`tool_max_count` bigint(20) DEFAULT NULL,
`tool_max_id` varchar(25) DEFAULT NULL,
`cus_max_count` bigint(20) DEFAULT NULL,
`cus_max_type` varchar(25) DEFAULT NULL,
`area_min_count` bigint(20) DEFAULT NULL,
`area_min_name` varchar(50) DEFAULT NULL,
`dot_min_count` bigint(20) DEFAULT NULL,
`dot_min_name` varchar(50) DEFAULT NULL,
`route_min_count` bigint(20) DEFAULT NULL,
`route_min_id` varchar(25) DEFAULT NULL,
`tool_min_count` bigint(20) DEFAULT NULL,
`tool_min_id` varchar(25) DEFAULT NULL,
`cus_min_count` bigint(20) DEFAULT NULL,
`cus_min_type` varchar(25) DEFAULT NULL,
`dt` varchar(25) NOT NULL,
PRIMARY KEY (`dt`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

   使用Sqoop工具导出
#!/bin/bash
source /etc/profile
#定义sqoop命令位置，Hive命令位置，在hadoop2
#sqoop=/opt/cloudera/parcels/CDH/bin/sqoop
#Hive=/opt/cloudera/parcels/CDH/bin/hive
#定义工作日期

#编写导入数据通用方法 接收两个参数：第一个：表名，第二个：查询语句
export_data(){
sqoop export \
--connect "jdbc:mysql://hadoop1:3306/tbl_metrics?
useUnicode=true&characterEncoding=utf-8" \
--username root \
--password 12345678 \
--table $1 \
--export-dir /user/hive/warehouse/lg_ads.db/$2/ \
--num-mappers 1 \
--input-fields-terminated-by ',' \
--update-mode allowinsert \
--update-key dt
}
# 导出快递单指标数据
export_lg_waybill_metrics(){
export_data waybill_agg_metrics waybill_metrics
}
#导出数据方法
export_lg_waybill_metrics  
   
   