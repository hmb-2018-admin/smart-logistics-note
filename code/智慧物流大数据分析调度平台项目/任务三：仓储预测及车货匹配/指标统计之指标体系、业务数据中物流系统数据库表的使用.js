 1.指标统计之指标体系
   
   快递单主题
       快递单量的统计主要是从多个不同的维度计算快递单量，从而监测公司快递业务运营情况。
   指标列表     维度
   最大快递单数 各类客户最大快递单数
                各渠道最大快递单数
                各网点最大快递单数
                各终端最大快递单数
   最小快递单数 各类客户最小快递单数
                各渠道最小快递单数
                各网点最小快递单数
                各终端最小快递单数
   
   运单主题
       运单统计根据区域、公司、网点、线路、运输工具等维度进行统计，可以对各个维度运单数
量进行排行，如对网点运单进行统计可以反映该网点的运营情况
   
   指标列表   维度
   最大运单数 最大区域运单数
              各分公司最大运单数
              各网点最大运单数
              各线路最大运单数
              各运输工具最大运单数
              各类客户最大运单数
   最小运单数 各区域最小运单数
              各分公司最小运单数
              各网点最小运单数
              各线路最小运单数
              各运输工具最小运单数
              各类客户最小运单数
   
   2.业务数据
     
	 1).物流系统数据库表
     揽件表(lg_collect_package)
	 字段名             字段描述
     id                 ID
     empId              快递员ID
     express_bill__id   快递单ID
     package_id         包裹ID
     state              揽件状态
     collect_package_dt 揽件时间
     cdt                创建时间
     udt                修改时间
     remark             备注
	 
	 客户表(lg_customer)
	 字段名                  字段描述
     id                      ID
     customer_nnumber        客户编号
     customer_name           客户姓名
     customer_tel            客户电话
     customer_mobile         客户手机
     customer_email          客户邮箱
     customer_type           客户类型ID
     is_own_reg              是否自行注册
     customer_reg_dt         注册时间
     customer_reg_channel_id 注册渠道ID
     customer_state_id       客户状态ID
     cdt                     创建时间
     udt                     修改时间
     last_login_dt           最后登录时间
     remark                  备注
	 
	 物流系统码表(lg_codes)
	 字段名                字段描述
     id                    ID
     business_implications 业务含义
     business_code         业务码
     business_val          业务码值
     business_val_type     业务码值类型
     cdt                   创建时间
     udt                   修改时间
     remark                备注
	 
	 快递单据表(lg_express_bill)
	 字段名                     字段描述
     id                         ID
     express_number             快递单号
     customer_id                客户ID
     employee_id                员工ID
     order_channel_id           下单渠道ID
     order_dt                   下单时间
     order_terminal_type        下单设备类型ID
     order_terminal_os_type     下单设备操作系统ID
     reserve_dt                 预约取件时间
     is_collect_package_timeout 是否取件超时
     timeout_dt                 超时时间
     express_bill_type          快递单生成方式
     cdt                        创建时间
     udt                        修改时间
     remark                     备注
	 
	 客户地址表(lg_customer_address)
	 字段名      字段描述
     id          ID
     name        用户名
     tel         电话
     mobile      手机
     detail_addr 详细地址
     area_id     区域ID
     gis_addr    gis地址
     addr_type   地址类型
     cdt         创建时间
     udt         修改时间
     remark      备注
	 
	 网点表(lg_dot)
	 字段名          字段描述
     id              ID
     dot_number      网点编号
     dot_name        网点名称
     dot_addr        网点地址
     dot_gis_addr    网点GIS地址
     dot_tel         网点电话
     company_id      网点所属公司ID
     manage_area_id  网点管理辖区ID
     manage_area_gis 网点管理辖区地理围栏
     state           网点状态
     cdt             创建时间
     udt             修改时间
     remark          备注
	 
	 公司表(lg_company)
	 字段名           字段描述
     id               ID
     company_name     公司名称
     city_id          城市ID
     company_number   公司编号
     company_addr     公司地址
     company_addr_gis 公司gis地址
     company_tel      公司电话
     is_sub_company   母公司ID
     state            公司状态
     cdt              创建时间
     udt              修改时间
     remark           备注
	 
	 公司网点关联表(lg_company_dot_map)
	 字段名     字段描述
     id         ID
     company_id 公司ID
     dot_id     网点ID
     cdt        创建时间
     udt        修改时间
     remark     备注
	 
	 运单表(lg_waybill)
	 字段名                     字段描述
     id                         ID
     waybill_number             运单号
     customer_id                寄件信息ID
     employee_id                收件员工ID
     order_channel_id           下单渠道ID
     order_dt                   下单时间
     order_terminal_type        下单设备类型ID
     order_terminal_os_type     下单设备操作系统ID
     reserve_dt                 预约取件时间
     is_collect_package_timeout 是否取件超时
     pkg_id                     订装ID
     pkg_number                 订装编号
     timeout_dt                 超时时间
     transform_type             运输方式
     delivery_customer_name     发货人
     delivery_addr              发货地址
     delivery_mobile            发货人手机
     delivery_tel               发货人电话
     receive_customer_name      收货人
     receive_addr               收货地址
     receive_mobile             收货人手机
     receive_tel                收货人电话
     cdt                        创建时间
     udt                        修改时间
     remark                     运单备注
	 
	 线路表(lg_route)
	 字段名                 字段描述
     id                     ID
     start_station          运输线路起点
     start_warehouse_id     起点仓库
     stop_station           运输线路终点
     stop_warehouse_id      终点仓库
     transport_kilometre_km 运输里程
     transport_dt_hour      运输时长
     transport_route_state  线路状态
     cdt                    创建时间
     udt                    修改时间
     remark                 备注
	 
	 运输工具表(lg_transport_tool)
	 字段名        字段描述
     id            ID
     brand         运输工具品牌
     model         运输工具型号
     type          运输工具类型
     given_load    额定载重
     load_cn_unit  中文载重单位
     load_en_unit  英文载重单位
     buy_dt        购买时间
     license_plate 牌照
     state         运输工具状态
     cdt           创建时间
     udt           修改时间
     remark        备注
	 
	 转运记录表(lg_transport_record)
	 字段名                字段描述
     id                    ID
     pw_id                 入库表ID
     pw_waybill_id         入库运单ID
     pw_waybill_number     入库运单号
     ow_id                 出库表ID
     ow_waybill_id         出库运单ID
     ow_waybill_number     出库运单号
     sw_id                 起点仓库ID
     ew_id                 终点仓库ID
     driver1_id            车辆驾驶员
     driver2_id            车辆跟车员1
     driver3_id            车辆跟车员2
     route_id              运输路线ID
     distance              运输里程
     duration              运输耗时
     start_vehicle_dt      发车时间
     predict_arrivals_dt   预计到达时间
     actual_arrivals_dt    实际达到时间
     cdt                   创建时间
     udt                   修改时间
     remark                备注
	 
	 包裹表(lg_pkg)
	 字段名        字段描述
     id            ID
     pw_bill       订装箱号
     pw_dot_id     操作人ID
     warehouse_id  所属仓库ID
     cdt           创建时间
     udt           修改时间
     remark        备注
	 
	 运单路线明细表(lg_waybill_line)
	 字段名             字段描述
     id                 ID
     waybill_number     运单号
     route_id           路线ID
     serial_number      序号
     transport_tool     运输工具ID
     delivery_record_id 发车记录单ID
     cdt                创建时间
     udt                修改时间
     remark             备注
	 
	 快递员表(lg_courier)
	 字段名             字段描述
     id                 ID
     job_num            快递员工号
     name               快递员姓名
     birathday          快递员生日
     tel                快递员联系电话
     pda_num            PDA编号
     car_id             车辆ID
     postal_standard_id 收派标准ID
     work_time_id       收派时间ID
     dot_id             网点ID
     state              快递员状态
     cdt                创建时间
     udt                修改时间
     remark             备注
	 
	 区域表(lg_areas)
	 字段名    字段描述
     id        ID
     name      区域名称
     type      区域类型
     pid       父区域ID
     zipcode   区域邮编
     order_no  排序字段
     name_en   区域英文首字母
     is_zxs    是否直辖市
     cdt       创建时间
     udt       修改时间
     remark    备注
	 
	 