DROP TABLE IF EXISTS `bus_info`;
CREATE TABLE `bus_info` (
  `lglat` varchar(255) DEFAULT NULL,
  `deployNum` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

SET FOREIGN_KEY_CHECKS = 1;