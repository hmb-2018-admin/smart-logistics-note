package com.lg.bean

//接收各个字段
case class BusInfo(
                     deployNum: String,
                     lglat: String
                   )

object BusInfo {
  def apply(data: String): BusInfo = {
    //获取一条消息，按照逗号切分，准备各个字段数据获取businfo对象
    val arr: Array[String] = data.split(",")
    if (arr.length == 2) {
      BusInfo(
        arr(0),
        arr(1)
      )
    } else {
      null
    }
  }
}
