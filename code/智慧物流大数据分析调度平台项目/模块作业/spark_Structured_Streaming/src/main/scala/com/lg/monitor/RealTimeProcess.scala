package com.lg.monitor

import com.lg.bean.BusInfo
import com.lg.monitor.MysqlWriter.MysqlWriter
import org.apache.spark.sql.{DataFrame, Dataset, SparkSession}

/**
 * 使用结构化流读取kafka中的数据
 */
object RealTimeProcess {
  def main(args: Array[String]): Unit = {
    System.setProperty("HADOOP_USER_NAME", "root") //权限问题
    //1.获取sparksession
    val spark: SparkSession = SparkSession.builder()
      .master("local[*]")
      .appName(RealTimeProcess.getClass.getName)
      .getOrCreate()
    spark.sparkContext.setLogLevel("WARN")
    import spark.implicits._
    //2.定义读取kafka数据源
    val kafkaDF: DataFrame = spark.readStream
      .format("kafka")
      .option("kafka.bootstrap.servers", "linxu122:9092,linxu123:9092")
      .option("subscribe", "lagou_bus_mysql_info")
      .load()

    //3.处理数据
    val kafkaValDf: DataFrame = kafkaDF.selectExpr("CAST(value AS STRING)")
    //转为ds
    val kafkaDs: Dataset[String] = kafkaValDf.as[String]
    //解析出经纬度数据，写入mysql
    //封装为一个case class：方便后续获取指定字段的数据
    val busInfoDs: Dataset[BusInfo] = kafkaDs.map(BusInfo(_))
    //把经纬度数据写入mysql
    busInfoDs.writeStream
      .foreach(new MysqlWriter)
      .outputMode("append")
      .start()

    spark.streams.awaitAnyTermination()
  }
}
