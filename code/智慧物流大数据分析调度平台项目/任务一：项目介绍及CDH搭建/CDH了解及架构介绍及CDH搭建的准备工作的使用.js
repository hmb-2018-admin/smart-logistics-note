 1.cloudera manager简单介绍
   
   Cloudera Manager是一个拥有集群自动化安装、中心化管理、集群监控、报警功能的一个工具（软件）,使得安装集
群从几天的时间缩短在几个小时内，运维人员从数十人降低到几人以内，极大的提高集群管理的效率。

 2.cloudera manager主要核心功能
   
   管理：对集群进行管理，如添加、删除节点等操作。
   监控：监控集群的健康情况，对设置的各种指标和系统运行情况进行全面监控。
   诊断：对集群出现的问题进行诊断，对出现的问题给出建议解决方案。
   集成：多组件进行整合
 
 3.cloudera manager 的架构
 4.准备云服务器5台
   
   需要配置较高的机器，如果电脑内存小于32G可以搭建CM成功但是不能支持我们项目所需所有软件都安装运行。谨
慎选择！！
   hadoop2、hadoop2、hadoop3、hadoop4、hadoop5
 
 5.准备cloudera安装包
   
   由于是离线部署，因此需要预先下载好需要的文件。
   需要准备的文件有:
   Cloudera Manager 5
   文件名: cloudera-manager-centos7-cm5.14.0_x86_64.tar.gz
   下载地址: https://archive.cloudera.com/cm5/cm/5/
   CDH安装包（Parecls包）
   版本号必须与Cloudera Manager相对应
   下载地址: https://archive.cloudera.com/cdh5/parcels/5.14.0/
   需要下载下面3个文件：
   CDH-5.14.0-1.cdh5.14.0.p0.23-el7.parcel
   CDH-5.14.0-1.cdh5.14.0.p0.23-el7.parcel.sha1
   manifest.json
   MySQL jdbc驱动
   文件名: mysql-connector-java-.tar.gz
   下载地址: https://dev.mysql.com/downloads/connector/j/
   解压出: mysql-connector-java-bin.jar
 
 6.所有机器安装安装jdk
 7.所有机器安装依赖包
 yum -y install chkconfig python bind-utils psmisc libxslt zlib sqlite cyrus-sasl-plain
cyrus-sasl-gssapi fuse portmap fuse-libs redhat-lsb
 
 8.安装mysql数据库
   
   随机选择一台机器，计划在hadoop2安装mysql数据库
   Mysql数据库信息
   安装节点：hadoop2
   用户：root
   密码：12345678