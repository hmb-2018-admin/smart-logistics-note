 1.数据导出
   
   针对仓储预测准备相关数据
   根据算法部门的要求，仓储预测模型需要提供4个数据集
   sales_train.csv;items.csv;item_categories.csv;entreports.
       sales_train
       历史销售数据，有日期字段，月份,仓库，商品，价格和日销售量
       date       block_num entrepot_id item_id   item_price item_cnt_day
   0   2017-01-02     0          59       22154     999.00      1.0
   1   2017-01-02     0          25       2552      899.00      1.0
   2   2017-01-02     0          25       2552      899.00      1.0
   3   2017-01-02     0          25       2552     1709.05      1.0
   4   2017-01-02     0          25       2552     1099.00      1.0
   统计出的是每个仓库每个商品的日销量数据，
       items.csv
       表示的是商品信息，有商品名称，仓库id,商品分类id
       entrepots.csv
       仓库数据集，仓库id,仓库名称
       item_category
       来源商品分类表，其中有商品分类id以及分类的名称
   1).导出销量数据
   观察sales数据，其中有商品id,商品价格，商品销量数据，这些字段可以从订单明细表中获取，
此外还需要提供仓库id，这个字段需要从订单仓库关联表中获取
   最后关于月份的编号需要额外进行处理获取；
#创建tmp层
create database lg_tmp;
# 订单明细表关联订单仓库表，基于订单id与商品id
use lg_tmp;
drop table if exists lg_tmp.tmp_order_item_entrepot;

create table tmp_order_item_entrepot
as 
select t1.itemsid as itemId,
t1.itemsname as itemName,
t2.entrepotId as entrepotId,
t1.itemsNum as itemNum,
t1.itemsPrice as itemPrice,
t1.dt as date1 
from
lg_ods.lg_order_items t1
join
lg_ods.lg_order_entrepot t2
on t1.orderId=t2.orderId
and
t1.itemsid =t2.itemid;
#对上面结果数据进行汇总，按照天统计每个商品的销量
use lg_tmp;
drop table if exists lg_tmp.tmp_order_item_entrepot_day;
create table tmp_order_item_entrepot_day
as
select
itemid,
entrepotid,
date1,
sum(itemNum) as itemNum,
itemprice
from lg_tmp.tmp_order_item_entrepot
group by
itemId,
entrepotid,
date1,
itemprice;

#验证结果
select * from lg_tmp.tmp_order_item_entrepot_day limit 5;
#增加月份信息 使用排名函数，需要对数据按照月份排序打上序号
获取月份数据
drop table if exists tmp_order_item_entrepot_month;
create table tmp_order_item_entrepot_month as
select itemid,entrepotid,itemnum,itemprice,date1,substr(date1,0,6) as month from
lg_tmp.tmp_order_item_entrepot_day ;
对月份进行排序
create table lg_tmp.tmp_order_item_rank
as
select 
itemid,
entrepotid,
itemnum,
itemprice,
date1,
dense_rank() over(order by month ) rank
from lg_tmp.tmp_order_item_entrepot_month;
#导出为csv文件
hive -e "set hive.cli.print.header=true; select date1,rank as block_num,
entrepotid as entrepot_id,itemid as item_id,itemprice as item_price, itemnum as
item_cnt_day from lg_tmp.tmp_order_item_rank ;" 
|grep -v "WARN" | sed 's/\t/,/g' >> /sales.csv

   2).导出商品数据
   观察商品数据特征，有商品名称，仓库id,商品分类id
   基于lg_tmp.tmp_order_item_entrepot获取
#lg_tmp.tmp_order_item_entrepot与商品表进行关联（获取全部数据不用考虑拉链表实现）
drop table if exists lg_tmp.tmp_items;
create table lg_tmp.tmp_items
as
select
t1.itemname as itemname,
t1.itemid as itemid,
t2.goodscatid as catid
from
lg_tmp.tmp_order_item_entrepot t1
join
lg_dim.lg_dim_items t2
on t1.itemid =t2.goodsid ;
#导出为csv文件
hive -e "set hive.cli.print.header=true; select * from lg_tmp.tmp_items ;"
|grep -v "WARN" | sed 's/\t/,/g' >> /items.csv
   3).导出仓库及商品分类数据
   直接从相关表中导出即可。
#导出仓库数据
hive -e "set hive.cli.print.header=true; select entrepotname,entrepotid from
lg_ods.lg_entrepots ;" |grep -v "WARN" | sed 's/\t/,/g' >> /entrepots.csv

#导出商品分类数据
hive -e "set hive.cli.print.header=true; 
select t2.catname as type,t1.catName as subtype,
t1.catId as item_category_id
from (select catId,parentId,catName from lg_ods.lg_item_cats where cat_level = 3
) t1 JOIN
(select catId,catName from lg_ods.lg_item_cats where cat_level = 2) t2
on t1.parentId =t2.catId ;" 
|grep -v "WARN" | sed 's/\t/,/g' >> /item_categories.csv
   