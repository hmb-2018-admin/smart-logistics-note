 1.数据ETL
   
   ods-->dwd
   lg_ods.lg_orders:订单表;
   lg_ods.lg_order_items:订单明细表
   lg_order_entrepot:订单仓库关联表
   订单表是周期性事实表；为保留订单状态，使用拉链表进行处理；
   订单明细表是普通的事实表，不涉及到状态变化和保留； 如果有数据清洗、数据转换的业务需求，使用
每日增量同步到dwd层即可；
   订单仓库关联表是普通的事实表，不涉及到状态变化和保留； 如果有数据清洗、数据转换的业务需求，
使用每日增量同步到dwd层即可；
   如果没有数据清洗、数据转换的业务需求，保留在ODS，不做任何变化。
   ods-->dim
   lg_ods.lg_entrepots:仓库表
   lg_ods.lg_items:商品表
   lg_ods.lg_item_cats:商品分类表
   仓库表，商品表，商品分类表都是作为维度表存在，使用拉链表进行处理；
   创建对应数据库
   --创建DIM层数据库
   create database lg_dim;
   备注：
  dwd,dws,dim层表底层均以parquet格式，因为底层使用spark引擎，spark程序对parquet格式的
数据处理效率更好。
  与维表不同，订单事实表的记录数非常多 订单有生命周期；
	  订单的状态不可能永远处于变化之中（订单的生命周期一般在15天左 右）
 订单是一个拉链表，而且是分区表 ；分区的目的：订单一旦终止，不会重复计算 ；分区的条
件：订单创建日期；保证相同笔订单存储在同一个分区

 2.ODS->DWD
   
   1).lg_dwd.fact_orders--订单拉链表
   创建DWD层订单表
   -- 创建DWD层订单表--拉链表
drop table if exists `lg_dwd`.`fact_orders`;
CREATE TABLE lg_dwd.fact_orders (
orderId bigint,
orderNo string,
userId bigint,
orderStatus bigint,
itemsMoney double,
deliverType bigint,
deliverMoney double,
totalMoney double,
realTotalMoney double,
payType bigint,
isPay bigint,
areaId bigint,
userAddressId bigint,
areaIdPath string,
userName string,
userAddress string,
userPhone string,
orderScore bigint,
isInvoice bigint,
invoiceClient string,
orderRemarks string,
orderSrc bigint,
needPay double,
payRand bigint,
orderType bigint,
isRefund bigint,
isAppraise bigint,
cancelReason bigint,
rejectReason bigint,
rejectOtherReason string,
isClosed bigint,
itemsSearchKeys string,
orderunique string,
 isFromCart string,
receiveTime string,
deliveryTime string,
tradeNo string,
dataFlag bigint,
createTime string,
settlementId bigint,
commissionFee double,
scoreMoney double,
useScore bigint,
orderCode string,
extraJson string,
orderCodeTargetId bigint,
noticeDeliver bigint,
invoiceJson string,
lockCashMoney double,
payTime string,
isBatch bigint,
totalPayFee bigint,
modifytime string,
start_date string,
end_date string
) partitioned by (dt string) STORED AS PARQUET ;

-- 创建DWD层临时订单表--拉链表
drop table if exists `lg_dwd`.`tmp_fact_orders`;
CREATE TABLE lg_dwd.tmp_fact_orders (
orderId bigint,
orderNo string,
userId bigint,
orderStatus bigint,
itemsMoney double,
deliverType bigint,
deliverMoney double,
totalMoney double,
realTotalMoney double,
payType bigint,
isPay bigint,
areaId bigint,
userAddressId bigint,
areaIdPath string,
userName string,
userAddress string,
userPhone string,
orderScore bigint,
isInvoice bigint,
invoiceClient string,
orderRemarks string,
orderSrc bigint,
needPay double,
payRand bigint,
orderType bigint,
isRefund bigint,
isAppraise bigint,
cancelReason bigint,
rejectReason bigint,
rejectOtherReason string,
isClosed bigint,
itemsSearchKeys string,
orderunique string,
isFromCart string,
receiveTime string,
deliveryTime string,
tradeNo string,
dataFlag bigint,
createTime string,
settlementId bigint,
commissionFee double,
scoreMoney double,
useScore bigint,
orderCode string,
extraJson string,
orderCodeTargetId bigint,
noticeDeliver bigint,
invoiceJson string,
lockCashMoney double,
payTime string,
isBatch bigint,
totalPayFee bigint,
modifytime string,
start_date string,
end_date string
) partitioned by (dt string) STORED AS PARQUET ;
   
   订单拉链操作
   (1).第一次导入拉链表
   开启Hive的动态分区，并根据数据的createtime字段进行分区划分，同一天创建的订单放在同一分
区！！
   #开启动态分区，默认是false
   #开启允许所有分区都是动态的，否则必须要有静态分区才能使用
   set hive.exec.dynamic.partition=true; 
   set hive.exec.dynamic.partition.mode=nonstrict;
   订单表数据：ODS层导入DWD层
   (1) 之前全部历史数据进入拉链表
insert overwrite table lg_dwd.fact_orders partition(dt)
select
orderid,
orderno,
userid,
orderstatus,
itemsmoney,
delivertype,
delivermoney,
totalmoney,
realtotalmoney,
paytype,
ispay,
areaid,
useraddressid,
areaidpath,
username,
useraddress,
userphone,
orderscore,
isinvoice,
invoiceclient,
orderremarks,
ordersrc,
needpay,
payrand,
ordertype,
isrefund,
isappraise,
cancelreason,
rejectreason,
rejectotherreason,
isclosed,
itemssearchkeys,
orderunique,
isfromcart,
receivetime,
deliverytime,
tradeno,
dataflag,
createtime,
settlementid,
commissionfee,
scoremoney,
usescore,
ordercode,
extrajson,
ordercodetargetid,
noticedeliver,
invoicejson,
lockcashmoney,
paytime,
isbatch,
totalpayfee,
modifytime,
--增加开始时间
date_format(modifytime,'yyyy-MM-dd') as start_date,
--增加结束时间
'9999-12-31' as end_date,
--指定动态分区使用的字段，动态分区的用法：就是查询字段的最后一个字段Hive表进行解析然后存入指定分区
--此次数据分区按照订单的创建时间
date_format(createtime,'yyyyMMdd')
from lg_ods.lg_orders where dt="20200609";
   (2).拉链表与每日合并
insert overwrite table lg_dwd.tmp_fact_orders partition(dt)
--新增数据的更新
select
orderid,
orderno,
userid,
orderstatus,
itemsmoney,
delivertype,
delivermoney,
totalmoney,
realtotalmoney,
paytype,
ispay,
areaid,
useraddressid,
areaidpath,
username,
useraddress,
userphone,
orderscore,
isinvoice,
invoiceclient,
orderremarks,
ordersrc,
needpay,
payrand,
ordertype,
isrefund,
isappraise,
cancelreason,
rejectreason,
rejectotherreason,
isclosed,
itemssearchkeys,
orderunique,
isfromcart,
receivetime,
deliverytime,
tradeno,
dataflag,
createtime,
settlementid,
commissionfee,
scoremoney,
usescore,
ordercode,
extrajson,
ordercodetargetid,
noticedeliver,
invoicejson,
lockcashmoney,
paytime,
isbatch,
totalpayfee,
modifytime,
--增加开始时间
date_format(modifyTime,'yyyy-MM-dd') as start_date,
--增加结束时间
'9999-12-31' as end_date,
--指定动态分区使用的字段，动态分区的用法：就是查询字段的最后一个字段Hive表进行解析然后存入指定分区
--此次数据分区按照订单的创建时间
date_format(createtime,'yyyyMMdd') as part
from lg_ods.lg_orders where dt="20200610"

union all

--历史拉链表更新数据
select
dw.orderid as orderid,
dw.orderno as orderno,
dw.userid as userid,
dw.orderstatus as orderstatus,
dw.itemsmoney as itemsmoney,
dw.delivertype as delivertype,
dw.delivermoney as delivermoney,
dw.totalmoney as totalmoney,
dw.realtotalmoney as realtotalmoney,
dw.paytype as paytype,
dw.ispay as ispay,
dw.areaid as areaid,
dw.useraddressid as useraddressid,
dw.areaidpath as areaidpath,
dw.username as username,
dw.useraddress as useraddress,
dw.userphone as userphone,
dw.orderscore as orderscore,
dw.isinvoice as isinvoice,
dw.invoiceclient as invoiceclient,
dw.orderremarks as orderremarks,
dw.ordersrc as ordersrc,
dw.needpay as needpay,
dw.payrand as payrand,
dw.ordertype as ordertype,
dw.isrefund as isrefund,
dw.isappraise as isappraise,
dw.cancelreason as cancelreason,
dw.rejectreason as rejectreason,
dw.rejectotherreason as rejectotherreason,
dw.isclosed as isclosed,
dw.itemssearchkeys as itemssearchkeys,
dw.orderunique as orderunique,
dw.isfromcart as isfromcart,
dw.receivetime as receivetime,
dw.deliverytime as deliverytime,
dw.tradeno as tradeno,
dw.dataflag as dataflag,
dw.createtime as createtime,
dw.settlementid as settlementid,
dw.commissionfee as commissionfee,
dw.scoremoney as scoremoney,
dw.usescore as usescore,
dw.ordercode as ordercode,
dw.extrajson as extrajson,
dw.ordercodetargetid as ordercodetargetid,
dw.noticedeliver as noticedeliver,
dw.invoicejson as invoicejson,
dw.lockcashmoney as lockcashmoney,
dw.paytime as paytime,
dw.isbatch as isbatch,
dw.totalpayfee as totalpayfee,
dw.modifytime as modifytime,
dw.start_date as start_date,
--修改end_date
case when ods.orderid is not null and dw.end_date ='9999-12-31'
then '2020-06-09'
else dw.end_date
end as end_date,
--动态分区需要的字段
dw.dt as part
from
lg_dwd.fact_orders dw
left join
(select * from lg_ods.lg_orders where dt ='20200610') ods
on dw.orderid=ods.orderid ;
   
   注意：使用union all要保证两个子查询得到的字段名称一致！！
   临时表中数据插入拉链表中
   insert overwrite table lg_dwd.fact_orders partition(dt) select * from
lg_dwd.tmp_fact_orders ;
   
   2).lg_order_items--订单明细表
   对于订单明细表,订单仓库信息的ETL来说，由于该表中数据是作为订单表的补充不会变化，也不涉及
到数据清洗，数据处理等需求，所以ods->dwd可以不做。直接从ods层获取数据即可。

 3.ODS-->DIM
   
   主要以下三张表需要从ODS转到DIM层，并且使用拉链表保存。
  lg_ods.lg_entrepots:仓库表
  lg_ods.lg_items:商品表
  lg_ods.lg_item_cats:商品分类表
   仓库表，商品表，商品分类表都是作为维度表存在，使用拉链表进行处理；
   1).lg_dim.lg_items 商品表
   创建DIM层商品表
use lg_dim;
DROP TABLE IF EXISTS `lg_dim`.`lg_dim_items`;
CREATE TABLE `lg_dim`.`lg_dim_items`(
goodsId bigint,
goodsSn string,
productNo string,
goodsName string,
goodsImg string,
shopId bigint,
goodsType bigint,
marketPrice double,
shopPrice double,
warnStock bigint,
goodsStock bigint,
goodsUnit string,
goodsTips string,
isSale bigint,
isBest bigint,
isHot bigint,
isNew bigint,
isRecom bigint,
goodsCatIdPath string,
goodsCatId bigint,
shopCatId1 bigint,
shopCatId2 bigint,
brandId bigint,
goodsDesc string,
goodsStatus bigint,
saleNum bigint,
saleTime string,
visitNum bigint,
appraiseNum bigint,
isSpec bigint,
gallery string,
goodsSeoKeywords string,
illegalRemarks string,
dataFlag bigint,
createTime string,
isFreeShipping bigint,
goodsSerachKeywords string,
modifyTime string,
start_date string,
end_date string
)
STORED AS PARQUET;
  
   
   (1).商品拉链表操作
   1 、第一次导入拉链表
insert overwrite table `lg_dim`.`lg_dim_items`
select
itemsid as goodsId, 
itemssn as goodsSn, 
productno as productNo, 
itemsname as goodsName, 
itemsimg as goodsImg, 
entrepotid as shopId, 
itemstype as goodsType, 
marketprice as marketPrice, 
entrepotprice as shopPrice, 
warnstock as warnStock, 
itemsstock as goodsStock, 
itemsunit as goodsUnit, 
itemstips as goodsTips, 
issale as isSale, 
isbest as isBest, 
ishot as isHot, 
isnew as isNew, 
isrecom as isRecom, 
itemscatidpath as goodsCatIdPath, 
itemscatid as goodsCatId, 
entrepotcatid1 as shopCatId1, 
entrepotcatid2 as shopCatId2,
brandid as brandId,
itemsdesc as goodsDesc,
itemsstatus as goodsStatus,
salenum as saleNum, 
saletime as saleTime,
visitnum as visitNum,
appraisenum as appraiseNum,
isspec as isSpec,
gallery as gallery,
itemsseokeywords as goodsSeoKeywords,
illegalremarks as illegalRemarks,
dataflag as dataFlag,
createtime as createTime,
isfreeshipping as isFreeShipping, 
itemsserachkeywords as goodsSerachKeywords, 
modifytime as modifyTime,
case when modifyTime is not null
then from_unixtime(unix_timestamp(modifyTime, 'yyyy-MM-dd HH:mm:ss'),'yyyy-MM-dd')
else from_unixtime(unix_timestamp(createTime, 'yyyy-MM-dd HH:mm:ss'),'yyyy-MM-dd')
end as start_date,
'9999-12-31' as end_date
from
`lg_ods`.`lg_items`
where dt = '20200609';   
   2 、每日合并拉链表
-- 将历史数据 当日数据合并加载到临时表
drop table if exists `lg_dim`.`tmp_lg_dim_items`;
create table `lg_dim`.`tmp_lg_dim_items`
as
select
dim.goodsId,
dim.goodsSn,
dim.productNo,
dim.goodsName,
dim.goodsImg,
dim.shopId,
dim.goodsType,
dim.marketPrice,
dim.shopPrice,
dim.warnStock,
dim.goodsStock,
dim.goodsUnit,
dim.goodsTips,
dim.isSale,
dim.isBest,
dim.isHot,
dim.isNew,
dim.isRecom,
dim.goodsCatIdPath,
dim.goodsCatId,
dim.shopCatId1,
dim.shopCatId2,
dim.brandId,
dim.goodsDesc,
dim.goodsStatus,
dim.saleNum,
dim.saleTime,
dim.visitNum,
dim.appraiseNum,
dim.isSpec,
dim.gallery,
dim.goodsSeoKeywords,
dim.illegalRemarks,
dim.dataFlag,
dim.createTime,
dim.isFreeShipping,
dim.goodsSerachKeywords,
dim.modifyTime,
dim.start_date,
case when dim.end_date >= '9999-12-31' and ods.itemsid is not null
then '2020-06-09'
else dim.end_date
end as end_date
from
`lg_dim`.`lg_dim_items` dim
left join
(select * from `lg_ods`.`lg_items` where dt='20200610') ods
on dim.goodsId = ods.itemsid
union all
select
itemsid as goodsId, 
itemssn as goodsSn, 
productno as productNo, 
itemsname as goodsName, 
itemsimg as goodsImg, 
entrepotid as shopId, 
itemstype as goodsType, 
marketprice as marketPrice, 
entrepotprice as shopPrice, 
warnstock as warnStock, 
itemsstock as goodsStock, 
itemsunit as goodsUnit, 
itemstips as goodsTips, 
issale as isSale, 
isbest as isBest, 
ishot as isHot, 
isnew as isNew, 
isrecom as isRecom, 
itemscatidpath as goodsCatIdPath, 
itemscatid as goodsCatId, 
entrepotcatid1 as shopCatId1, 
entrepotcatid2 as shopCatId2, 
brandid as brandId, 
itemsdesc as goodsDesc, 
itemsstatus as goodsStatus, 
salenum as saleNum, 
saletime as saleTime, 
visitnum as visitNum, 
appraisenum as appraiseNum, 
isspec as isSpec, 
gallery as gallery, 
itemsseokeywords as goodsSeoKeywords, 
illegalremarks as illegalRemarks, 
dataflag as dataFlag, 
createtime as createTime, 
isfreeshipping as isFreeShipping, 
itemsserachkeywords as goodsSerachKeywords, 
modifytime as modifyTime,
case when modifyTime is not null
then from_unixtime(unix_timestamp(modifyTime, 'yyyy-MM-dd
HH:mm:ss'),'yyyy-MM-dd')
else from_unixtime(unix_timestamp(createTime, 'yyyy-MM-dd HH:mm:ss'),
'yyyy-MM-dd')
end as start_date,
'9999-12-31' as end_date
from
`lg_ods`.`lg_items`
where dt = '20200610';
   
   临时表中数据插入拉链表
   insert overwrite table lg_dim.lg_dim_items select * from
lg_dim.tmp_lg_dim_items;
   2).lg_dim.dim_product_cat 商品分类表
   lg_dim.dim_product_cat表为商品分类数据，在数仓中使用拉链表存储。
   创建商品分类拉链表
drop table if exists lg_dim.dim_product_cat;
create table lg_dim.dim_product_cat(
catId bigint,
parentId bigint,
catName string,
isShow bigint,
isFloor bigint,
catSort bigint,
dataFlag bigint,
createTime string,
commissionRate double,
catImg string,
subTitle string,
simpleName string,
seoTitle string,
seoKeywords string,
seoDes string,
catListTheme string,
detailTheme string,
mobileCatListTheme string,
mobileDetailTheme string,
wechatCatListTheme string,
wechatDetailTheme string,
cat_level bigint,
modifyTime string,
start_date string,
end_date string
)
STORED AS PARQUET;

drop table if exists lg_dim.tmp_dim_product_cat;
create table lg_dim.tmp_dim_product_cat(
catId bigint,
parentId bigint,
catName string,
isShow bigint,
isFloor bigint,
catSort bigint,
dataFlag bigint,
createTime string,
commissionRate double,
catImg string,
subTitle string,
simpleName string,
seoTitle string,
seoKeywords string,
seoDes string,
catListTheme string,
detailTheme string,
mobileCatListTheme string,
mobileDetailTheme string,
wechatCatListTheme string,
wechatDetailTheme string,
cat_level bigint,
modifyTime string,
start_date string,
end_date string
)
STORED AS PARQUET;
   分类表拉链操作
   (1). 第一次导入拉链表
insert overwrite table lg_dim.dim_product_cat
select 
catid,
parentid,
catname,
isshow,
isfloor,
catsort,
dataflag,
createtime,
commissionrate,
catimg,
subtitle,
simplename,
seotitle,
seokeywords,
seodes,
catlisttheme,
detailtheme,
mobilecatlisttheme,
mobiledetailtheme,
wechatcatlisttheme,
wechatdetailtheme,
cat_level,
modifytime,
case when modifyTime is not null
then from_unixtime(unix_timestamp(modifyTime,'yyyy-MM-dd HH:mm:ss'),'yyyy-MM-dd')
else from_unixtime(unix_timestamp(createTime,'yyyy-MM-dd HH:mm:ss'),'yyyy-MM-dd')
end as start_date,
'9999-12-31' as end_date
from
`lg_ods`.`lg_item_cats`
where dt='20200609';
   (2).每日合并拉链表
drop table if exists lg_dim.tmp_dim_product_cat;
create table lg_dim.tmp_dim_product_cat as
select
dim.catid,
dim.parentid,
dim.catname,
dim.isshow,
dim.isfloor,
dim.catsort,
dim.dataflag,
dim.createtime,
dim.commissionrate,
dim.catimg,
dim.subtitle,
dim.simplename,
dim.seotitle,
dim.seokeywords,
dim.seodes,
dim.catlisttheme,
dim.detailtheme,
dim.mobilecatlisttheme,
dim.mobiledetailtheme,
dim.wechatcatlisttheme,
dim.wechatdetailtheme,
dim.cat_level,
dim.modifytime,
dim.start_date,
case when dim.end_date >= '9999-12-31' and ods.catid is not null
then '2020-06-09'
else dim.end_date
end as end_date
from
`lg_dim`.`dim_product_cat` dim
left join
(select * from `lg_ods`.`lg_item_cats` where dt='20200610') ods
on dim.catid = ods.catid
union all
select
catid,
parentid,
catname,
isshow,
isfloor,
catsort,
dataflag,
createtime,
commissionrate,
catimg,
subtitle,
simplename,
seotitle,
seokeywords,
seodes,
catlisttheme,
detailtheme,
mobilecatlisttheme,
mobiledetailtheme,
wechatcatlisttheme,
wechatdetailtheme,
cat_level,
modifytime,
case when modifyTime is not null
then from_unixtime(unix_timestamp(modifyTime, 'yyyy-MM-dd HH:mm:ss'),'yyyy-MM-dd')
else from_unixtime(unix_timestamp(createTime, 'yyyy-MM-dd HH:mm:ss'),'yyyy-MM-dd')
end as start_date,
'9999-12-31' as end_date
from
`lg_ods`.`lg_item_cats`
where dt = '20200610';
   
   导入拉链表
insert overwrite table lg_dim.dim_product_cat select * from lg_dim.tmp_dim_product_cat ;   
   3).创建仓库拉链表
   创建仓库拉链表
drop table if exists `lg_dim`.`dim_lg_entrepots`;
CREATE TABLE `lg_dim`.`dim_lg_entrepots` (
entrepotId bigint,
areaId bigint,
entrepotName string,
entrepotkeeper string,
telephone string,
entrepotImg string,
entrepotTel string,
entrepotQQ string,
entrepotAddress string,
invoiceRemarks string,
serviceStartTime bigint,
serviceEndTime bigint,
freight bigint,
entrepotAtive int,
entrepotStatus int,
statusDesc string,
dataFlag int,
createTime string ,
modifyTime string,
start_date string,
end_date string
) STORED AS PARQUET;

drop table if exists `lg_dim`.`tmp_dim_lg_entrepots`;
CREATE TABLE `lg_dim`.`tmp_dim_lg_entrepots` (
entrepotId bigint,
areaId bigint,
entrepotName string,
entrepotkeeper string,
telephone string,
entrepotImg string,
entrepotTel string,
entrepotQQ string,
entrepotAddress string,
invoiceRemarks string,
serviceStartTime bigint,
serviceEndTime bigint,
freight bigint,
entrepotAtive int,
entrepotStatus int,
statusDesc string,
dataFlag int,
createTime string ,
modifyTime string,
start_date string,
end_date string
) STORED AS PARQUET;

   仓库表拉链操作
   (1).第一次导入拉链表
insert overwrite table lg_dim.dim_lg_entrepots
select
entrepotId,
areaId,
entrepotName,
entrepotkeeper,
telephone,
entrepotImg,
entrepotTel,
entrepotQQ,
entrepotAddress,
invoiceRemarks,
serviceStartTime,
serviceEndTime,
freight,
entrepotAtive,
entrepotStatus,
statusDesc,
dataFlag,
createTime,
modifyTime,
case when modifyTime is not null
then from_unixtime(unix_timestamp(modifyTime, 'yyyy-MM-dd HH:mm:ss'),'yyyy-MM-dd')
else from_unixtime(unix_timestamp(createTime, 'yyyy-MM-dd HH:mm:ss'),'yyyy-MM-dd')
end as start_date,
'9999-12-31' as end_date
from
`lg_ods`.`lg_entrepots`
where dt = '20200609';   
   (2).每日合并拉链表
drop table if exists lg_dim.tmp_dim_product_cat;
create table lg_dim.tmp_dim_product_cat as
select
dim.catid,
dim.parentid,
dim.catname,
dim.isshow,
dim.isfloor,
dim.catsort,
dim.dataflag,
dim.createtime,
dim.commissionrate,
dim.catimg,
dim.subtitle,
dim.simplename,
dim.seotitle,
dim.seokeywords,
dim.seodes,
dim.catlisttheme,
dim.detailtheme,
dim.mobilecatlisttheme,
dim.mobiledetailtheme,
dim.wechatcatlisttheme,
dim.wechatdetailtheme,
dim.cat_level,
dim.modifytime,
dim.start_date,
case when dim.end_date >= '9999-12-31' and ods.catid is not null
then '2020-06-09'
else dim.end_date
end as end_date
from
`lg_dim`.`dim_product_cat` dim
left join
(select * from `lg_ods`.`lg_item_cats` where dt='20200610') ods
on dim.catid = ods.catid
union all
select
catid,
parentid,
catname,
isshow,
isfloor,
catsort,
dataflag,
createtime,
commissionrate,
catimg,
subtitle,
simplename,
seotitle,
seokeywords,
seodes,
catlisttheme,
detailtheme,
mobilecatlisttheme,
mobiledetailtheme,
wechatcatlisttheme,
wechatdetailtheme,
cat_level,
modifytime,
case when modifyTime is not null
then from_unixtime(unix_timestamp(modifyTime, 'yyyy-MM-dd HH:mm:ss'),'yyyy-MM-dd')
else from_unixtime(unix_timestamp(createTime, 'yyyy-MM-dd HH:mm:ss'),
'yyyy-MM-dd')
end as start_date,
'9999-12-31' as end_date
from
`lg_ods`.`lg_item_cats`
where dt = '20200610';
   导入拉链表
insert overwrite table lg_dim.dim_lg_entrepots select * from lg_dim.tmp_dim_lg_entrepots ;   